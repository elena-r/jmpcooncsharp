﻿using System;
using jmpcoon.Model.Physics;

namespace jmpcoon.Model.Entities
{
    public abstract class AbstractEntity : IEntity
    {
        private readonly IPhysicalBody body;

        public AbstractEntity(IPhysicalBody body)
        {
            this.body = body ?? throw new ArgumentNullException();
        }

        public Tuple<Double, Double> Position => this.body.Position;

        public BodyShape Shape => this.body.Shape;

        public double Angle => this.body.Angle;

        public abstract EntityType Type { get; }

        public EntityState State => this.body.State;

        public Tuple<double, double> Dimensions => this.body.Dimensions;

        public Tuple<double, double> Velocity => this.body.Velocity;

        public IPhysicalBody PhysicalBody => this.body;

        public bool IsAlive() => this.body.Exists();

        public void Print()
        {
            Console.WriteLine("Type: {0}; Shape: {1}; Position: ({2}, {3}); " +
                "Dimensions: {4}x{5}; Angle: {6}", Type, Shape, Position.Item1,
                Position.Item2, Dimensions.Item1, Dimensions.Item2, Angle);
        } 
    }
}
