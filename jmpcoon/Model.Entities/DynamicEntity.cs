﻿using System;
using jmpcoon.Model.Physics;
namespace jmpcoon.Model.Entities
{
    public abstract class DynamicEntity : AbstractEntity
    {
        public DynamicEntity(DynamicPhysicalBody body) : base(body) {}
    }
}
