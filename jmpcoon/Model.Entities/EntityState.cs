﻿using System;
namespace jmpcoon.Model.Entities
{
    public enum EntityState
    {
        CLIMBING_DOWN,
        CLIMBING_UP,
        JUMPING,
        MOVING_LEFT,
        MOVING_RIGHT,
        IDLE
    }
}
