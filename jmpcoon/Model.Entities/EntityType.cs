﻿using System;
namespace jmpcoon.Model.Entities
{
    public enum EntityType
    {
        ENEMY_GENERATOR,
        LADDER,
        PLAYER,
        PLATFORM,
        POWERUP,
        ROLLING_ENEMY,
        WALKING_ENEMY
    }
}
