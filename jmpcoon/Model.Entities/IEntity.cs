﻿using System;
using jmpcoon.Model.Physics;

namespace jmpcoon.Model.Entities
{
    public interface IEntity
    {
        Tuple<Double, Double> Position { get; }
        BodyShape Shape { get; }
        double Angle { get; }
        EntityType Type { get; }
        EntityState State { get; }
        bool IsAlive();
        Tuple<Double, Double> Dimensions { get; }
        Tuple<Double, Double> Velocity { get; }
        IPhysicalBody PhysicalBody { get; }
    }
}
