﻿using System;
namespace jmpcoon.Model.Entities
{
    public enum MovementType
    {
        CLIMB_DOWN,
        CLIMB_UP,
        JUMP,
        MOVE_RIGHT,
        MOVE_LEFT
    }
}
