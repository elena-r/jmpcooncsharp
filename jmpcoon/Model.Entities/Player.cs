﻿using System;
using jmpcoon.Model.Physics;
namespace jmpcoon.Model.Entities
{
    public class Player : DynamicEntity
    {
        private readonly PlayerPhysicalBody body;
        public Player(PlayerPhysicalBody body) : base(body)
        {
            this.body = body;
        }

        public override EntityType Type => EntityType.PLAYER;

        public void Move(MovementType movement)
        {
            this.body.ApplyMovement(movement, Values(movement).Item1, Values(movement).Item2);
        }

        public int Lives => this.body.Lives;

        private Tuple<double, double> Values(MovementType movement)
        {
            double? x;
            double? y;
            switch(movement)
            {
                case MovementType.CLIMB_DOWN:
                    x = 0; y = -0.6; break;
                case MovementType.CLIMB_UP:
                    x = 0; y = 0.6; break;
                case MovementType.JUMP:
                    x = 0; y = 11; break;
                case MovementType.MOVE_LEFT:
                    x = -1.1; y = 0; break;
                case MovementType.MOVE_RIGHT:
                    x = 1.1; y = 0; break;
                default:
                    x = null; y = null; break;
            }
            return new Tuple<double, double>(x ?? throw new ArgumentNullException(), 
                                             y ?? throw new ArgumentNullException());
        }
    }
}
