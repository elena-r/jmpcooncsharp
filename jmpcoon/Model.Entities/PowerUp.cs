﻿using System;
using jmpcoon.Model.Physics;
namespace jmpcoon.Model.Entities
{
    public class PowerUp : StaticEntity
    {
        private readonly PowerUpType powerUpType;

        public PowerUp(StaticPhysicalBody body, PowerUpType powerUpType) : base(body)
        {
            this.powerUpType = powerUpType;
        }

        public override EntityType Type => EntityType.POWERUP;

        public PowerUpType PowerType => this.powerUpType;
    }
}
