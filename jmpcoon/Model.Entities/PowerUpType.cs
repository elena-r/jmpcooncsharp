﻿using System;

namespace jmpcoon.Model.Entities
{
    public enum PowerUpType
    {
        GOAL,
        EXTRA_LIFE,
        INVINCIBILITY
    }
}
