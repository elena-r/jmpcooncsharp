﻿using System;
using jmpcoon.Model.Physics;
namespace jmpcoon.Model.Entities
{
    public abstract class StaticEntity : AbstractEntity
    {
        protected internal StaticEntity(StaticPhysicalBody body) : base(body) {}
    }
}
