﻿using System;
namespace jmpcoon.Model.Physics
{
    public abstract class AbstractPhysicalBody : IPhysicalBody
    {
        private readonly Body body;

        public AbstractPhysicalBody(Body body)
        {
            this.body = body ?? throw new ArgumentNullException();
        }

        public Tuple<Double,Double> Position
        {
            get { return this.body.Position; }
        }

        public double Angle
        {
            get { return this.body.Angle; }
        }

        public BodyShape Shape
        {
            get { return this.body.Shape; }
        }

        public bool Exists() => this.body.Active;

        public Tuple<double, double> Velocity
        {
            get { return this.body.Velocity; }
        }

        public Tuple<double, double> Dimensions
        {
            get { return this.body.Dimensions; }
        }

        public abstract Model.Entities.EntityState State { get; }
    }
}
