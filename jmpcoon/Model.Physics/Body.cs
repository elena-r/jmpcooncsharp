﻿using System;
namespace jmpcoon.Model.Physics
{
    /*
     * This is a replica of the physics library (dyn4j) Body class
     */
    public class Body
    {
        private Tuple<double, double> position;
        private Tuple<double, double> velocity = new Tuple<double, double>(0, 0);
        private Tuple<double, double> dimensions;
        private double? angle = null;
        private double gravityScale = 1;
        private double damping = 0;
        private BodyShape? bodyShape = null;
        private bool active;

        public Body()
        {
            this.active = true;
        }

        public Tuple<double, double> Position
        {
            get { return this.position ?? throw new ArgumentNullException(); }
            set { this.position = value; }
        }

        public Tuple<double, double> Velocity
        {
            get { return this.velocity; }
            set { this.velocity = value; }
        }

        public Tuple<double, double> Dimensions
        {
            get { return this.dimensions ?? throw new ArgumentNullException(); }
            set { this.dimensions = value; }
        }

        public double Angle
        {
            get { return this.angle ?? 0; }
            set { this.angle = value; }
        }

        public BodyShape Shape
        {
            get { return this.bodyShape ?? throw new ArgumentNullException(); }
            set { this.bodyShape = value; }
        }

        public double GravityScale
        {
            get { return this.gravityScale; }
            set { this.gravityScale = value; }
        }

        public double Damping
        {
            get { return this.damping; }
            set { this.gravityScale = value; }
        }

        public bool Active
        {
            get { return this.active; }
            set { this.active = value; }
        }

        public void ApplyImpulse(double x, double y)
        {
            this.Position = new Tuple<double, double>(this.Position.Item1 + x, this.Position.Item2 + y);
            this.Velocity = new Tuple<double, double>(Velocity.Item1 + x, Velocity.Item2 + y);
        }
    }
}
