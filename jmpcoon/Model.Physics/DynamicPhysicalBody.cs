﻿using System;
using jmpcoon.Model.Entities;

namespace jmpcoon.Model.Physics
{
    public class DynamicPhysicalBody : AbstractPhysicalBody
    {
        private static readonly double PRECISION = 0.1;
        private static readonly double MAXVELOCITY_X = 1;
        private static readonly double MAXVELOCITY_Y = 0.5;
        private static readonly double CLIMB_DAMPING = 7;

        private double maxVelocityX = MAXVELOCITY_X;
        private double maxVelocityY = MAXVELOCITY_Y;
        private readonly Body body;
        private EntityState currentState;

        public DynamicPhysicalBody(Body body) : base(body)
        { 
            this.body = body;
            currentState = EntityState.IDLE;
        }

        public override EntityState State
        {
            get 
            {   
                if (Math.Abs(this.body.Velocity.Item1) <= PRECISION &&
                    Math.Abs(this.body.Velocity.Item2) <= PRECISION &&
                    this.currentState != EntityState.CLIMBING_DOWN &&
                    this.currentState != EntityState.CLIMBING_UP)
                {
                    return EntityState.IDLE;
                }
                return this.currentState; 
            }
        }

        public void SetIdle()
        {
            this.currentState = EntityState.IDLE;
            this.body.GravityScale = 1;
            this.body.Damping = 0;
            this.body.Velocity = new Tuple<double, double>(0, 0);
        }

        public void ApplyMovement(MovementType movement, double x, double y)
        {
            if (this.currentState != EntityState.CLIMBING_DOWN &&
                this.currentState != EntityState.CLIMBING_UP &&
                (movement == MovementType.CLIMB_UP || 
                 movement == MovementType.CLIMB_DOWN))
            {
                this.body.GravityScale = 0;
                this.body.Damping = CLIMB_DAMPING;
                this.body.Velocity = new Tuple<double, double>(0, 0);
            }
            this.currentState = Convert(movement);
            this.body.ApplyImpulse(x, y);
            if (Math.Abs(this.body.Velocity.Item1) > this.maxVelocityX)
            {
                this.body.Velocity = new Tuple<double, double>(Math.Sign(this.body.Velocity.Item1) * this.maxVelocityX, 
                                                                this.body.Velocity.Item2);
            }
            if (Math.Abs(this.body.Velocity.Item2) > this.maxVelocityY)
            {
                this.body.Velocity = new Tuple<double, double>(this.body.Velocity.Item1,
                                                                Math.Sign(this.body.Velocity.Item2) * this.maxVelocityY);
            }

        }

        public void SetFixedVelocity(MovementType movement, double x, double y)
        {
            this.currentState = Convert(movement);
            this.body.Velocity = new Tuple<double, double>(x, y);
        }

        protected internal void SetMaxVelocity(double multiplierX, double multiplierY)
        {
            this.maxVelocityX = MAXVELOCITY_X * multiplierX;
            this.maxVelocityY = MAXVELOCITY_Y * multiplierY;
        }

        private EntityState Convert(MovementType movement)
        {
            switch(movement)
            {
                case MovementType.CLIMB_DOWN:
                    return EntityState.CLIMBING_DOWN;
                case MovementType.CLIMB_UP:
                    return EntityState.CLIMBING_UP;
                case MovementType.JUMP:
                    return EntityState.JUMPING;
                case MovementType.MOVE_LEFT:
                    return EntityState.MOVING_LEFT;
                case MovementType.MOVE_RIGHT:
                    return EntityState.MOVING_RIGHT;
                default:
                    return EntityState.IDLE;
            }
        }


    }
}
