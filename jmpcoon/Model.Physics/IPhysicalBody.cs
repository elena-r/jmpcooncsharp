﻿using System;
using jmpcoon.Model.Entities;

namespace jmpcoon.Model.Physics
{
    public interface IPhysicalBody
    {
        Tuple<Double, Double> Position { get; }

        double Angle { get;}

        EntityState State { get; }

        bool Exists();

        BodyShape Shape { get; }

        Tuple<Double, Double> Dimensions { get; }

        Tuple<Double, Double> Velocity { get; }
    }
}
