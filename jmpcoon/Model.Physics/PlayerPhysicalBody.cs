﻿using System;
using jmpcoon.Model.Entities;
namespace jmpcoon.Model.Physics
{
    public class PlayerPhysicalBody : DynamicPhysicalBody
    {
        private static readonly double INVINCIBILITY_VELOCITY_X = 2;
        private static readonly double INVINCIBILITY_VELOCITY_Y = 2;

        private readonly Body body;
        private bool invincible;
        private bool invulnerable;
        private int lives;

        public PlayerPhysicalBody(Body body) : base(body)
        {
            this.body = body;
            this.invincible = false;
            this.invulnerable = false;
            this.lives = 1;
        }

        public bool IsInvulnerable() => this.invulnerable;

        public bool IsInvincible() => this.invincible;

        public void GivePowerUp(PowerUpType powerUpType)
        {
            if (powerUpType == PowerUpType.EXTRA_LIFE)
            {
                this.lives++;
            } else if (powerUpType == PowerUpType.INVINCIBILITY)
            {
                this.invincible = true;
                this.ModifyMaxVelocity(INVINCIBILITY_VELOCITY_X, INVINCIBILITY_VELOCITY_Y);
            }
        }

        public int Lives => this.lives;

        public void Hit()
        {
            if (!this.invulnerable)
            {
                this.lives--;
                this.invulnerable = true;
            }
            if (this.lives == 0)
            {
                this.Kill();
            }
        }

        public void Kill()
        {
            this.lives = 0;
            this.body.Active = false;
        }

        public void EndInvincibility()
        {
            this.invincible = false;
            this.ModifyMaxVelocity(1, 1);
        }

        public void EndInvulnerability()
        {
            this.invulnerable = false;
        }

        private void ModifyMaxVelocity(double multiplierX, double multiplierY)
         => this.SetMaxVelocity(multiplierX, multiplierY);
    }
}
