﻿using System;
using jmpcoon.Model.Entities;

namespace jmpcoon.Model.Physics
{
    public class StaticPhysicalBody : AbstractPhysicalBody
    {
        public StaticPhysicalBody(Body body) : base(body) { }

        public override EntityState State => EntityState.IDLE;
    }
}
