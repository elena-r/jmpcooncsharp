﻿using NUnit.Framework;
using System;
using jmpcoon.Model.Entities;
using jmpcoon.Model.Physics;

namespace jmpcoon.Tests
{
    [TestFixture()]
    public class NUnitTestClass
    {
        [Test()]
        public void PlayerCreation()
        {
            Assert.That(()=> PlayerCreate(), Throws.Nothing);
        }

        [Test()]
        public void PowerupCreation()
        {
            Assert.That(() =>{ PowerUp extraLife = new PowerUp(new StaticPhysicalBody(GenericBodyCreate()), PowerUpType.EXTRA_LIFE);}, Throws.Nothing);
        }

        [Test()]
        public void PlayerMovement()
        {
            Player player = PlayerCreate();
            Tuple<double, double> initPosition = player.Position;
            player.Move(MovementType.MOVE_LEFT);
            Tuple<double, double> newPosition = player.Position;
            Assert.Greater(initPosition.Item1, newPosition.Item1);
            Assert.AreEqual(initPosition.Item2, newPosition.Item2);

            initPosition = newPosition;
            player.Move(MovementType.MOVE_RIGHT);
            newPosition = player.Position;
            Assert.Less(initPosition.Item1, newPosition.Item1);
            Assert.AreEqual(initPosition.Item2, newPosition.Item2);

            initPosition = newPosition;
            player.Move(MovementType.CLIMB_UP);
            newPosition = player.Position;
            Assert.AreEqual(initPosition.Item1, newPosition.Item1);
            Assert.Greater(newPosition.Item2, initPosition.Item2);

            initPosition = newPosition;
            player.Move(MovementType.CLIMB_DOWN);
            newPosition = player.Position;
            Assert.AreEqual(initPosition.Item1, newPosition.Item1);
            Assert.Less(newPosition.Item2, initPosition.Item2);
        }

        [Test()]
        public void UninitializedBody()
        {
            Body playerBody = new Body();
            PlayerPhysicalBody playerPhysicalBody = new PlayerPhysicalBody(playerBody);
            Assert.Throws<ArgumentNullException>(()=>
            {
                Tuple<double, double> pos = playerPhysicalBody.Position;
            });
            Assert.Throws<ArgumentNullException>(() =>
            {
                Tuple<double, double> dim = playerPhysicalBody.Dimensions;
            });
        }

        [Test()]
        public void InvincibilityPowerup()
        {
            PlayerPhysicalBody player = new PlayerPhysicalBody(GenericBodyCreate());
            Assert.IsFalse(player.IsInvincible());
            player.GivePowerUp(PowerUpType.INVINCIBILITY);
            Assert.IsTrue(player.IsInvincible());
        }

        [Test()]
        public void ExtraLifePowerup()
        {
            PlayerPhysicalBody player = new PlayerPhysicalBody(GenericBodyCreate());
            Assert.AreEqual(player.Lives, 1);
            player.GivePowerUp(PowerUpType.EXTRA_LIFE);
            Assert.AreEqual(player.Lives, 2);
        }

        [Test()]
        public void PlayerDeath()
        {
            Body playerBody = GenericBodyCreate();
            PlayerPhysicalBody player = new PlayerPhysicalBody(playerBody);
            Assert.IsTrue(playerBody.Active);
            player.Hit();
            Assert.IsFalse(playerBody.Active);
        }

        private Player PlayerCreate()
        {
            PlayerPhysicalBody playerPhysicalBody = new PlayerPhysicalBody(GenericBodyCreate());
            Player player = new Player(playerPhysicalBody);
            return player;
        }

        private Body GenericBodyCreate()
        {
            Body body = new Body();
            body.Active = true;
            body.Dimensions = new Tuple<double, double>(0.5, 0.5);
            body.Position = new Tuple<double, double>(1, 0);
            body.Shape = BodyShape.RECTANGLE;
            return body;
        }
    }
}
