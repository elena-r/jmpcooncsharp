﻿using System;
using jmpcoon.Model.Entities;
using jmpcoon.Model.Physics;
namespace jmpcoon.Tests
{
    public class Test
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("PLAYER CREATION");
            Body playerBody = new Body();
            playerBody.Active = true;
            playerBody.Dimensions = new Tuple<double, double>(2, 3);
            playerBody.Position = new Tuple<double, double>(0, 0);
            playerBody.Shape = BodyShape.RECTANGLE;
            PlayerPhysicalBody playerPhysicalBody = new PlayerPhysicalBody(playerBody);
            Player player = new Player(playerPhysicalBody);
            player.Print();

            /* movement test */
            Console.WriteLine("---Movement test");
            Console.WriteLine("Move player right");
            player.Move(MovementType.MOVE_RIGHT);
            Console.WriteLine(player.Position);

            Console.WriteLine("Move player down (climb down)");
            player.Move(MovementType.CLIMB_DOWN);
            Console.WriteLine(player.Position);


            Console.WriteLine();


            Console.WriteLine("POWERUP CREATION - EXTRA_LIFE");
            Body powerupBodyE = new Body();
            powerupBodyE.Active = true;
            powerupBodyE.Dimensions = new Tuple<double, double>(0.5, 0.5);
            powerupBodyE.Position = new Tuple<double, double>(1, 0);
            powerupBodyE.Shape = BodyShape.RECTANGLE;
            StaticPhysicalBody powerupPhysicalBodyE = new StaticPhysicalBody(powerupBodyE);
            PowerUp powerUpE = new PowerUp(powerupPhysicalBodyE, PowerUpType.EXTRA_LIFE);
            powerUpE.Print();


            Console.WriteLine();


            Console.WriteLine("POWERUP CREATION - INVINCIBILITY");
            Body powerupBodyI = new Body();
            powerupBodyI.Active = true;
            powerupBodyI.Dimensions = new Tuple<double, double>(1.5, 1);
            powerupBodyI.Position = new Tuple<double, double>(1, 1);
            powerupBodyI.Shape = BodyShape.RECTANGLE;
            StaticPhysicalBody powerupPhysicalBodyI = new StaticPhysicalBody(powerupBodyI);
            PowerUp powerUpI = new PowerUp(powerupPhysicalBodyI, PowerUpType.INVINCIBILITY);
            powerUpI.Print();

            Console.WriteLine();

            /* player lives test */
            Console.WriteLine("Player lives: {0}", player.Lives);
            Console.WriteLine("---Give extra life powerup to Player");
            playerPhysicalBody.GivePowerUp(PowerUpType.EXTRA_LIFE);
            Console.WriteLine("Player lives: {0}\n", player.Lives);

            /* powerups are deactivated when they collide with player tho*/
            Console.WriteLine("isPlayerInvincible: {0}", playerPhysicalBody.IsInvincible());
            Console.WriteLine("---Give invincibility powerup to Player");
            playerPhysicalBody.GivePowerUp(PowerUpType.INVINCIBILITY);
            Console.WriteLine("isPlayerInvincible: {0}", playerPhysicalBody.IsInvincible());
        }
    }
}
